//addName function takes 2 parameters
//putting 2 parameters adds it to the array 'successfully added'
//entering a parameter that already exists in the array gives 'Already exists'
//Key concepts: push, indexOf, if, 

let tuitt = ['Charles', 'Paul', 'Sef', 'Alex', 'Paul'];

function addName(name) {
    if (tuitt.indexOf(name) !== -1){
    	console.log(name + ' already exists');
    }else {
    	tuitt.push(name);
    	console.log(name + ' successfully added');
    	// console.log(tuitt);
    }

}
addName('Paul', tuitt)